﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WarehouseManagementBackend.ViewModels.Enums
{
    public class StatusName
    {
        public enum StatusesNames
        {
            Awaiting,
            InProgress,
            Done
        }
    }
}
