﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace WarehouseManagementBackend.ViewModels.ScreenViewModels
{
    public class ScreenViewModel
    {
        [Required]
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
