﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace WarehouseManagementBackend.ViewModels.TagViewModels
{
    public class TagUpdateViewModel
    {
        [Required]
        public int Id { get; set; }
        public string Name { get; set; }
        public int? ScreenId { get; set; }
    }
}
