﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using static WarehouseManagementBackend.ViewModels.Enums.StatusName;

namespace WarehouseManagementBackend.ViewModels.StatusViewModels
{
    public class StatusViewModel
    {
        [Required]
        public int Id { get; set; }
        public StatusesNames StatusEnumNumber { get; set; }
        public string StatusName { get; set; }
    }
}
