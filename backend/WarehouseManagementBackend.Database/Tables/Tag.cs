﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WarehouseManagementBackend.Database.Tables
{
    public class Tag : BaseEntity
    {
        public string Name { get; set; }

        [ForeignKey(nameof(Screen))]
        public int? ScreenId { get; set; }
        public virtual Screen Screen { get; set; }

        public virtual ICollection<Product> Products { get; set; }
        
    }
}
