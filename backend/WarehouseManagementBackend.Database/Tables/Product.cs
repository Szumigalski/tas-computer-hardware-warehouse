﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace WarehouseManagementBackend.Database.Tables
{
    public class Product : BaseEntity
    {
        public string Name { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }
        // TODO: Zrobić zapis obrazka do pliku 
        public string ImageLink { get; set; }

        [ForeignKey(nameof(Category))]
        public int CategoryId { get; set; }
        public virtual Category Category { get;set;}

        [ForeignKey(nameof(Tag))]
        public int? TagId { get; set; }
        public virtual Tag Tag { get; set; }

        public virtual ICollection<InOrder> InOrders { get; set; }
    }
}
