﻿using System;
using System.Collections.Generic;
using System.Text;
using WarehouseManagementBackend.Database.Repositories.Base;
using WarehouseManagementBackend.Database.Tables;

namespace WarehouseManagementBackend.Database.Repositories
{
    public class TagRepository: Repository<Tag>, IRepository<Tag>
    {
        public TagRepository(ApplicationDbContext _dbContext) : base(_dbContext)
        { }
    }
}
