﻿using System;
using System.Collections.Generic;
using System.Text;
using WarehouseManagementBackend.Database.Repositories.Base;
using WarehouseManagementBackend.Database.Tables;

namespace WarehouseManagementBackend.Database.Repositories
{
    public class ScreenRepository : Repository<Screen>, IRepository<Screen>
    {
        public ScreenRepository(ApplicationDbContext _dbContext) : base(_dbContext)
        { }
    }
}
