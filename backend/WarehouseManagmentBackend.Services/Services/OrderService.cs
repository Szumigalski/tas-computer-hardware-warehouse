﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WarehouseManagementBackend.Database.Repositories.Base;
using WarehouseManagementBackend.Database.Tables;
using WarehouseManagementBackend.Services.Base;
using WarehouseManagementBackend.Services.Interfaces;
using WarehouseManagementBackend.ViewModels.OrderViewModels;

namespace WarehouseManagementBackend.Services.Services
{
    public class OrderService : ServiceBase<OrderViewModel, Order>, IOrderService
    {
        public OrderService(IMapper mapper, IRepository<Order> repository) : base(mapper, repository)
        { }

        public async Task<Order> Create()
        {
            OrderViewModel orderModel = new OrderViewModel();
            orderModel.StartTime = DateTime.Now;
            orderModel.Status = false;
            orderModel.userName = "test";
            var result = Mapper.Map<Order>(orderModel);
            await Repository.Create(result);
            return result;
        }
    }
}
