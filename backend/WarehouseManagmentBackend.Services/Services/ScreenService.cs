﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using WarehouseManagementBackend.Database.Repositories.Base;
using WarehouseManagementBackend.Database.Tables;
using WarehouseManagementBackend.Services.Base;
using WarehouseManagementBackend.Services.Interfaces;
using WarehouseManagementBackend.ViewModels.ScreenViewModels;

namespace WarehouseManagementBackend.Services.Services
{
    public class ScreenService : ServiceBase<ScreenViewModel, Screen>, IScreenService
    {
        public ScreenService(IMapper mapper, IRepository<Screen> repository) : base(mapper, repository)
        { }
    }
}
