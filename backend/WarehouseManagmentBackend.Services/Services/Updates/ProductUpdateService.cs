﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using WarehouseManagementBackend.Database.Repositories.Base;
using WarehouseManagementBackend.Database.Tables;
using WarehouseManagementBackend.Services.Base;
using WarehouseManagementBackend.Services.Interfaces.Updates;
using WarehouseManagementBackend.ViewModels.ProductViewModels;

namespace WarehouseManagementBackend.Services.Services.Updates
{
    public class ProductUpdateService : ServiceBase<ProductUpdateViewModel, Product>, IProductUpdateService
    {
        public ProductUpdateService(IMapper mapper, IRepository<Product> repository) : base(mapper, repository)
        {

        }
    }
}
