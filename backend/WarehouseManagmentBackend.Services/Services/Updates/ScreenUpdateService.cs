﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using WarehouseManagementBackend.Database.Repositories.Base;
using WarehouseManagementBackend.Database.Tables;
using WarehouseManagementBackend.Services.Base;
using WarehouseManagementBackend.Services.Interfaces.Updates;
using WarehouseManagementBackend.ViewModels.ScreenViewModels;

namespace WarehouseManagementBackend.Services.Services.Updates
{
    public class ScreenUpdateService : ServiceBase<ScreenUpdateViewModel, Screen>, IScreenUpdateService
    {
        public ScreenUpdateService(IMapper mapper, IRepository<Screen> repository) : base(mapper, repository)
        {

        }
    }
}
