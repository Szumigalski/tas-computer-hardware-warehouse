﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using WarehouseManagementBackend.Database.Repositories.Base;
using WarehouseManagementBackend.Database.Tables;
using WarehouseManagementBackend.Services.Base;
using WarehouseManagementBackend.Services.Interfaces;
using WarehouseManagementBackend.ViewModels.TagViewModels;

namespace WarehouseManagementBackend.Services.Services
{
    public class TagService : ServiceBase<TagViewModel, Tag>, ITagService
    {
        public TagService(IMapper mapper, IRepository<Tag> repository) : base(mapper, repository)
        {

        }
    }
}
