﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using WarehouseManagementBackend.Database.Tables;

namespace WarehouseManagementBackend.Services.Base
{
    public interface IServiceBase<TViewModel,TEntity> : IDisposable
    {
        IEnumerable<TViewModel> Get(); 
        Task<TViewModel> Get(int id);
        Task Create(TViewModel viewModel);
        Task Update(int id, TViewModel viewModel);
        Task Delete(int id);
        Task SaveAsync();
    }
}
