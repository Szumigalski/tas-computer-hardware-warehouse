﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using WarehouseManagementBackend.Database.Tables;
using WarehouseManagementBackend.ViewModels.OrderViewModels;

namespace WarehouseManagmentBackend.Services.Profiles
{
    public class OrderProfile : Profile
    {
        public OrderProfile()
        {
            CreateMap<OrderViewModel, Order>();
            CreateMap<Order, OrderViewModel>();
        }
    }
}
