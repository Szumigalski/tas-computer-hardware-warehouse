﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using WarehouseManagementBackend.Database.Tables;
using WarehouseManagementBackend.ViewModels.StatusViewModels;

namespace WarehouseManagmentBackend.Services.Profiles
{
    public class StatusProfile : Profile
    {
        public StatusProfile()
        {
            CreateMap<StatusViewModel, Status>();
            CreateMap<Status, StatusViewModel>();
        }
    }
}
