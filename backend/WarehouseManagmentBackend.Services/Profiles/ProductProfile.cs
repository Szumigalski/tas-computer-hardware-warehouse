﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using WarehouseManagementBackend.Database.Tables;
using WarehouseManagementBackend.ViewModels.ProductViewModels;

namespace WarehouseManagmentBackend.Services.Profiles
{
    public class ProductProfile : Profile
    {
        public ProductProfile()
        {
            CreateMap<ProductViewModel, Product>();
            CreateMap<Product, ProductViewModel>();

            CreateMap<ProductUpdateViewModel, Product>();
            CreateMap<Product, ProductUpdateViewModel>();
        }
    }
}
