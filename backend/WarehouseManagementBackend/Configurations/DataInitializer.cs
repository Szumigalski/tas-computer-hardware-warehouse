﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WarehouseManagementBackend.Database.Tables;

namespace WarehouseManagementBackend.Configurations
{
    public static class DataInitializer
    {
        public static void SeedData(UserManager<ApplicationUser> userManager, IServiceProvider serviceProvider)
        {
            var roleManager = serviceProvider.GetRequiredService<RoleManager<IdentityRole>>();
            SeedRoles(roleManager).Wait();
            SeedUsers(userManager);
        }

        public static async Task SeedRoles(RoleManager<IdentityRole> roleManager)
        {
            string[] roleNames = { "admin", "employee", "user" };
            foreach ( var  roleName in roleNames)
            {
                var roleExist = await roleManager.RoleExistsAsync(roleName);
                if (!roleExist)
                {
                    await roleManager.CreateAsync(new IdentityRole(roleName));
                }
            }

        }

        public static void SeedUsers(UserManager<ApplicationUser> userManager)
        {
            if (userManager.FindByEmailAsync("admin@warehouse.pl").Result == null)
            {
                ApplicationUser admin = new ApplicationUser();
                admin.Email = "admin@warehouse.pl";
                admin.UserName = "admin@warehouse.pl";

                userManager.CreateAsync(admin, "Warehouse1").Wait();

                userManager.AddToRoleAsync(admin, "admin").Wait();
            }
            if (userManager.FindByEmailAsync("employee@warehouse.pl").Result == null)
            {
                ApplicationUser employee = new ApplicationUser();
                employee.Email = "employee@warehouse.pl";
                employee.UserName = "employee@warehouse.pl";

                userManager.CreateAsync(employee, "Warehouse1").Wait();

                userManager.AddToRoleAsync(employee, "employee").Wait();
            }
            if (userManager.FindByEmailAsync("user@warehouse.pl").Result == null)
            {
                ApplicationUser user = new ApplicationUser();
                user.Email = "user@warehouse.pl";
                user.UserName = "user@warehouse.pl";

                userManager.CreateAsync(user, "Warehouse1").Wait();

                userManager.AddToRoleAsync(user, "user");
            }
        }
    }
}
