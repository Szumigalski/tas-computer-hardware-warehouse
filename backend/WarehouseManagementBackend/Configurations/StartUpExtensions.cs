﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WarehouseManagementBackend.Tokens;

namespace WarehouseManagementBackend.Configurations
{
    public static class StartUpExtensions
    {
        /// <summary>
        /// Customized options to configure JWTBearer
        /// </summary>
        public static IServiceCollection AddCustomizedAuthentication(this IServiceCollection services, IConfiguration Configuration)
        {
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;

            })
             .AddJwtBearer(cfg =>
             {
                 cfg.RequireHttpsMetadata = false;
                 cfg.SaveToken = true;

                 var signingKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(Configuration.GetSection("TokenAuthentication:SecretKey").Value));
                 var tokenValidationParameters = new TokenValidationParameters
                 {
                     ValidateIssuerSigningKey = true,
                     IssuerSigningKey = signingKey,
                     /// <summary>
                     /// Validate the JWT Issuer (iss) claim
                     /// </summary>

                     ValidateIssuer = true,
                     ValidIssuer = Configuration.GetSection("TokenAuthentication:Issuer").Value,
                     /// <summary>
                     ///Validate the JWT Audience (aud) claim
                     /// </summary>
                     RequireExpirationTime = true,
                     RequireSignedTokens = true,
                     ValidateAudience = true,
                     ValidAudience = Configuration.GetSection("TokenAuthentication:Audience").Value,
                     /// <summary>
                     /// Validate the token expiry
                     /// </summary>

                     ValidateLifetime = true,
                     /// <summary>
                     /// If you want to allow a certain amount of clock drift, set that here:
                     /// </summary>
                     ClockSkew = TimeSpan.Zero
                 };

                 cfg.TokenValidationParameters = tokenValidationParameters;


             });
            return services;
        }
        /// <summary>
        /// Configuration of Entity Framework options
        /// </summary>

        public static IServiceCollection AddCustomizedEntityOptions(this IServiceCollection services)
        {
            services.Configure<IdentityOptions>(options => {
                options.Password.RequireNonAlphanumeric = false;

            });
            return services;
        }
        /// <summary>
        /// Configuration of Cookie options
        /// </summary>
        public static IServiceCollection AddCustomizedCookiesConfiguration(this IServiceCollection services)
        {
            services.ConfigureApplicationCookie(options =>
            {
                options.Cookie.HttpOnly = true;
                options.ExpireTimeSpan = TimeSpan.FromSeconds(15);//.FromMinutes(300);
                options.SlidingExpiration = true;
            });
            return services;
        }


        public static TokenProviderOptions GetTokenProviderOptions(IConfiguration Configuration)
        {
            var signingKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(Configuration.GetSection("TokenAuthentication:SecretKey").Value));
            //linijka z błędem argumentNullException ^^^^^
            var tokenProviderOptions = new TokenProviderOptions
            {
                Path = Configuration.GetSection("TokenAuthentication:TokenPath").Value,
                Audience = Configuration.GetSection("TokenAuthentication:Audience").Value,
                Issuer = Configuration.GetSection("TokenAuthentication:Issuer").Value,
                /// <summary>
                /// User can change Expiration time of tokens in appssettings.json
                /// </summary>
                Expiration = TimeSpan.Parse(Configuration.GetSection("TokenAuthentication:TokenLifeTime").Value),
                SigningCredentials = new SigningCredentials(signingKey, SecurityAlgorithms.HmacSha256),

            };
            return tokenProviderOptions;
        }

    }
}

