﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WarehouseManagementBackend.Services.Interfaces;
using WarehouseManagementBackend.Services.Interfaces.Updates;
using WarehouseManagementBackend.ViewModels.TagViewModels;

namespace WarehouseManagementBackend.Controllers
{
    [Route("api/tag")]
    [AllowAnonymous]
    public class TagController : Controller
    {
        private readonly ITagService _service;
        private readonly ITagUpdateService _updateService;

        public TagController(ITagService service, ITagUpdateService updateService)
        {
            _service = service;
            _updateService = updateService;
        }

        [HttpGet]
        public IActionResult Get()
        {
            return Json(_service.Get());
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            return Json(await _service.Get(id));
        }

        [HttpPost]
        public async Task<IActionResult> Create(TagViewModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            await _service.Create(model);
            await _service.SaveAsync();
            return Ok();
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Update(TagUpdateViewModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            await _updateService.Update(model.Id, model);
            await _updateService.SaveAsync();

            return Ok();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            await _service.Delete(id);
            await _service.SaveAsync();

            return Ok();
        }
    }
}