﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WarehouseManagementBackend.Services.Interfaces;
using WarehouseManagementBackend.ViewModels.StatusViewModels;

namespace WarehouseManagementBackend.Controllers
{
    [AllowAnonymous]
    [Route("api/status")]
    public class StatusController : Controller
    {
        private readonly IStatusService _service;

        public StatusController(IStatusService service)
        {
            _service = service;
        }

        [HttpGet]
        public IActionResult Get()
        {
            return Json(_service.Get());
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            return Json(await _service.Get(id));
        }

        [HttpPost]
        public async Task<IActionResult> Create(StatusViewModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            await _service.Create(model);
            await _service.SaveAsync();
            return Ok();
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Update(StatusViewModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            await _service.Update(model.Id, model);
            await _service.SaveAsync();

            return Ok();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            await _service.Delete(id);
            await _service.SaveAsync();

            return Ok();
        }
    }
}