﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WarehouseManagementBackend.Database.Tables;

namespace WarehouseManagementBackend.Controllers
{
    [Route("api/register")]
    [AllowAnonymous]
    public class UserController : Controller
    {
        private readonly ILogger<UserController> _logger;
        private readonly SignInManager<ApplicationUser> _signInManager;
        public UserManager<ApplicationUser> _userManager;

        public UserController( SignInManager<ApplicationUser> signInManager, UserManager<ApplicationUser> userManager, ILogger<UserController> logger)
        {
            _logger = logger;
            _signInManager = signInManager;
            _userManager = userManager;
        }

        [HttpGet]
        [Route("getuser")]
        public async Task<IActionResult> getUser(string token)
        {
            var handler = new JwtSecurityTokenHandler();
            var tokenS = handler.ReadJwtToken(token) as JwtSecurityToken;

            var userName = tokenS.Subject;

            return Json(await _userManager.FindByNameAsync(userName));
        }


        [HttpPost]
        //[Authorize(Roles ="admin")]
        [Route("admin")]
        public async Task<IActionResult> CreateAdmin(string firstname, string secondname, string password, string email, string username, string phonenumber)
        {
            if(ModelState.IsValid)
            {
                var user = new ApplicationUser { FirstName = firstname, SecondName = secondname, Email = email, UserName = username, PhoneNumber = phonenumber};
                var result = await _userManager.CreateAsync(user, password);
                var resultRole = await _userManager.AddToRoleAsync(user, "admin");
                if (result.Succeeded && resultRole.Succeeded)
                {
                    _logger.LogInformation("Stworzono Admina");
                    
                    return Ok();
                }
                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError(string.Empty, error.Description);
                }
            }
            return BadRequest();
        }

        [HttpPost]
        //[Authorize(Roles = "employee,admin")]
        [Route("employee")]
        public async Task<IActionResult> CreateEmployee(string firstname, string secondname, string password, string email, string username, string phonenumber)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser { FirstName = firstname, SecondName = secondname, Email = email, UserName = username , PhoneNumber = phonenumber};
                var result = await _userManager.CreateAsync(user, password);
                var resultRole = await _userManager.AddToRoleAsync(user, "employee");
                if (result.Succeeded && resultRole.Succeeded)
                {
                    _logger.LogInformation("Stworzono employee");
                    
                    return Ok();
                }
                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError(string.Empty, error.Description);
                }
            }
            return BadRequest();
        }

        [HttpPost]
        [Route("user")]
        public async Task<IActionResult> CreateUser(string firstname, string secondname, string password, string email, string username, string phonenumber)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser { FirstName = firstname, SecondName = secondname, Email = email, UserName = username ,PhoneNumber = phonenumber};
                var result = await _userManager.CreateAsync(user, password);
                var resultRole = await _userManager.AddToRoleAsync(user, "user");
                if (result.Succeeded && resultRole.Succeeded)
                {
                    _logger.LogInformation("Stworzono usera");
                    
                    return Ok();
                }
                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError(string.Empty, error.Description);
                }
            }
            return BadRequest();
        }

        [HttpPut]
        [Route("edituser")]
        public async Task<IActionResult> EditUser(ApplicationUser applicationUser)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByEmailAsync(applicationUser.Email);
                if(applicationUser.SecondName != null)
                    user.SecondName = applicationUser.SecondName;
                if (applicationUser.PhoneNumber != null)
                    user.PhoneNumber = applicationUser.PhoneNumber;
                if (applicationUser.FirstName != null)
                    user.FirstName = applicationUser.FirstName;
                if (applicationUser.UserName != null)
                    user.UserName = applicationUser.UserName;
                if (applicationUser.Email != null)
                    user.Email = applicationUser.Email;

                var result = await _userManager.UpdateAsync(user);
                if (result.Succeeded)
                {
                    _logger.LogInformation("User zedytowany");

                    return Ok();
                }
                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError(string.Empty, error.Description);
                }
            }
            return BadRequest();
        }
    }
}
