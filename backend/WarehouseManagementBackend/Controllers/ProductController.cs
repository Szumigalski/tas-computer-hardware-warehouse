﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WarehouseManagementBackend.Services.Interfaces;
using WarehouseManagementBackend.Services.Interfaces.Updates;
using WarehouseManagementBackend.ViewModels.ProductViewModels;

namespace WarehouseManagementBackend.Controllers
{
    [AllowAnonymous]
    [Route("api/product")]
    public class ProductController : Controller
    {
        private readonly IProductService _service;
        private readonly IProductUpdateService _updateService;

        public ProductController(IProductService service, IProductUpdateService updateService)
        {
            _service = service;
            _updateService = updateService;
        }

        [HttpGet]
        public IActionResult Get()
        {
            return Json(_service.Get());
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            return Json(await _service.Get(id));
        }

        [HttpGet("forCategory")]
        public async Task<IActionResult> GetForCategory(int id)
        {
            return Json( await _service.GetProductsWithCategoryId(id));
        }

        [HttpPost]
        public async Task<IActionResult> Create(ProductViewModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            await _service.Create(model);
            await _service.SaveAsync();
            return Ok();
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Update(ProductUpdateViewModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            await _updateService.Update(model.Id, model);
            await _updateService.SaveAsync();

            return Ok();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            await _service.Delete(id);
            await _service.SaveAsync();

            return Ok();
        }
    }
}
