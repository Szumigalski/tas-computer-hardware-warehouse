﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using WarehouseManagementBackend.Database.Tables;

namespace WarehouseManagementBackend.Controllers
{
    [AllowAnonymous]
    [Route("api/admin")]
    public class AdminController : Controller
    {
        public UserManager<ApplicationUser> _userManager;

        public AdminController(UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
        }

        [HttpGet]
        public IActionResult Get()
        {
            var screenList = new List<object>();
            screenList.Add(new { Link = "/api/", Name = "Ekran główny apliakcji, do którego jesteśmy przeniesieni po zalogowaniu" });
            return Json(screenList);
        }

        
        [HttpGet]
        //[Authorize(Roles ="admin")]
        [Route("employeeList")]
        public async Task<IActionResult> GetEmployeeList()
        {
            return Json( await _userManager.GetUsersInRoleAsync("employee"));
        }

        [HttpGet]
        //[Authorize(Roles ="admin")]
        [Route("adminList")]
        public async Task<IActionResult> GetAdminList()
        {
            return Json(await _userManager.GetUsersInRoleAsync("admin"));
        }

    }
}
