import React, {Component} from 'react';
import { StyleSheet} from 'react-native';
import { 
    Container, 
    Card,
    CardItem,
    Body, 
    Form,
    Item,
    Input,
    Label, 
    Button,
    Text } from 'native-base';
import {
    ImageBackground 
    } from 'react-native';
    import { StackActions, NavigationActions } from 'react-navigation'; // Version can be specified in package.json

export default class Login extends Component {
  static navigationOptions = {
    title: 'Login',
  }
  render() {
    const resizeMode = 'center'
    return (
      <Container>
        <ImageBackground 
          style={{
            flex: 1,
            resizeMode,
            position: 'absolute',
            width: '100%',
            height: '100%',
            justifyContent: 'center',
            alignItems: 'center'
          }}
          source={require('../img/fabrykaTlo.jpg')}
        >
            <Card style={styles.card}>
                <CardItem header bordered>
                  <Text style={{color: 'black'}}>Logowanie</Text>
                </CardItem>
                <CardItem bordered>
                  <Body bordered>
                      <Item stackedLabel style={{justifyContent: 'center', width: '100%'}}>
                        <Label>Login:</Label>
                        <Input style={{ paddingTop: -25, width: '90%' }} type='pass'/>
                      </Item>
                      <Item stackedLabel last style={{justifyContent: 'center', width: '100%'}}>
                        <Label>Hasło:</Label>
                        <Input 
                          style={{ paddingTop: -25, width: '90%' }} 
                          secureTextEntry={ true }
                          />
                      </Item>
                  </Body>
                </CardItem>
                <CardItem bordered style={{justifyContent: 'center'}}>
                  <Text style={{color: 'black'}}>Zaloguj jako:</Text>
                </CardItem>
                <CardItem footer bordered style={styles.buttonFooter}>
                  <Button 
                    info 
                    style={styles.button}
                    onPress={() => {
                      this.props.navigation.dispatch(StackActions.reset({
                        index: 0,
                        actions: [
                          NavigationActions.navigate({ routeName: 'AdminPanel' })
                        ],
                      }))
                    }}
                    >
                    <Text> Admin </Text>
                  </Button>
                  <Button dark style={styles.button}>
                    <Text> Pracownik </Text>
                  </Button>
                  <Button success style={styles.button}>
                    <Text> Klient </Text>
                  </Button>
                </CardItem>
            </Card>
        </ImageBackground >
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  card: {
    width: '90%'
  },
  buttonFooter: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'center'
  },
  button: {
    marginBottom: 10,
    marginLeft: 2,
    marginRight: 2
  }
});
