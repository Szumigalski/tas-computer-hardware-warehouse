import React, { Component } from 'react'
import {create} from 'apisauce'
import { StyleSheet, ListView} from 'react-native';
import { Container, Header, Content, Button, Icon, List, ListItem, Text } from 'native-base';
import ProductModal from './ProductModal'

export default class Products extends Component {
    constructor(props) {
        super(props);
        this.ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
        this.state = {
            basic: true,
            productList: [],
            modalVisible: false
        }
    }

    setModalVisible(){
        this.setState({
            modalVisible: !this.state.modalVisible
        });
      }

    componentDidMount(){
        this.getProducts()
    }

    getProducts(){
        var self = this
        const api = create({
            baseURL: 'https://waremana.projektstudencki.pl',
            headers: {'Accept': 'application/vnd.github.v3+json'}
          })
        api
          .get('/api/product')
          .then((response) => {
              console.log('lols',response)
              let products =[]
              response.data.map(product=>{
                products.push(
                    {
                        id: product.id,
                        name: product.name,
                        quantity: product.quantity,
                        price: product.price,
                        // category: self.getCategoryPath(product, self.state.categoryList),
                        categoryId: product.categoryId,
                        tag: product.tagId,
                        // tagName: self.getTagName(product.tagId),
                        imageLink: product.imageLink,
                        show: true
                    }
                )
              })
              self.setState({
                productList: products
            })
            }) 
    }

    render(){  
        const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
        return (
            <Container>
                <Content>
                <Button 
                    full 
                    success
                    onPress={()=>this.setModalVisible()}
                    >
                    <Text>Dodaj produkt</Text>
                </Button>
                <List
                    leftOpenValue={75}
                    rightOpenValue={-75}
                    dataSource={this.ds.cloneWithRows(this.state.productList)}
                    renderRow={data =>
                    <ListItem >
                        <Text > {data.name} </Text>
                    </ListItem>}
                    renderLeftHiddenRow={data =>
                    <Button full onPress={() => alert(data)}>
                        <Icon active name="information-circle" />
                    </Button>}
                    renderRightHiddenRow={(data, secId, rowId, rowMap) =>
                    <Button full danger onPress={() => alert(data)}>
                        <Icon active name="trash" />
                    </Button>}
                />
                </Content>
                <ProductModal 
                    setModalVisible={()=>this.setModalVisible()}
                    modalVisible={this.state.modalVisible}
                />
            </Container>
        )
    }
}

const styles = StyleSheet.create({
  headcell: {
    width: '50%'
  }
});