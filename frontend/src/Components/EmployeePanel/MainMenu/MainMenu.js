import React, {Component} from 'react'
import { Button, Menu, Header, Icon } from 'semantic-ui-react'
import { withRouter } from "react-router-dom"
import './MainMenu.scss'

class MainMenu extends Component {
  handleItemClick = (e, { name }) => 
    this.setState({ activeItem: name }, 
    this.setHistory(name)
  )

  setHistory(name) {
    if(name === 'logout'){
      this.props.history.push("/")
    } 
  }

  render(){
    return(
    <Menu color='black' inverted>
      <Menu.Item style={{width: 0, position: 'absolute', top: '12px'}}>
        <Icon name='warehouse' size='large'  style={{ position: 'absolute' }}/>
      </Menu.Item>
      <Menu.Item>
        <Header as='h2' style={{ color: 'white', marginLeft: '35px' }}> Computer Hardware Warehouse</Header>
      </Menu.Item>

        <Menu.Item position='right'>
          <Button 
            icon 
            inverted 
            color='blue' 
            labelPosition='right'
            style={{marginRight: 20}}
            >User<Icon name='user circle' size='large'/></Button>
          <Button 
            name='logout'
            icon 
            inverted 
            color='red' 
            onClick={this.handleItemClick}
            labelPosition='right'>Logut<Icon name='sign-out' size='large'/></Button>
        </Menu.Item>
      </Menu>
    )
  }
}

export default withRouter(MainMenu)
