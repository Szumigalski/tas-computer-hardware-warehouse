import React, { Component } from 'react'
import { Icon, Table, Header, Image, Modal, TableBody, GridRow, Button } from 'semantic-ui-react'


export default class BasketModal extends Component {


    state = {
        openBasket: false,
        itemsInBasket: []
    }

    ShowBasket = () => {
        // debugger
        this.setState({
            openBasket: true
        })
    }

    HiddenBasket = () => {
        this.setState({
            openBasket: false
        })
    }

    addToCart(index, name, price) {
        let itemAlreadyIn = false
        if (this.state.itemsInBasket.length > 0) {
            for (var i = 0; i < this.state.itemsInBasket.length; i++) {
                if (this.state.itemsInBasket[i].text === name) {
                    itemAlreadyIn = true
                    this.state.itemsInBasket[i].quantity += 1
                    break
                }
            }
            if (itemAlreadyIn === false) {
                this.state.itemsInBasket.push({ id: index, text: name, quantity: 1, price: price })
            }
        }
        else {
            this.state.itemsInBasket.push({ id: index, text: name, quantity: 1, price: price })
        }
        console.log(this.state.itemsInBasket)
    }
    removeItem(e) {
        let tmpTable = []
        for (var i = 0; i < this.state.itemsInBasket.length; i++) {
            if (this.state.itemsInBasket[i].id !== e.target.id) {
                tmpTable.push(this.state.itemsInBasket[i])
            }
        }
        this.setState({ itemsInBasket: tmpTable })
    }

    renderItems = () => {
        let table = []
        for (var i = 0; i < this.state.itemsInBasket.length; i++) {
            table.push(<Table.Row>
                <Table.Cell>
                    <Header as='h4'>
                        {this.state.itemsInBasket[i].text}
                    </Header>
                </Table.Cell>
                <Table.Cell>{this.state.itemsInBasket[i].quantity}</Table.Cell>
                <Table.Cell>{this.state.itemsInBasket[i].price}</Table.Cell>
                <Table.Cell><Button id={this.state.itemsInBasket[i].id} onClick={this.removeItem.bind(this)} icon><Icon name='trash alternate' /></Button></Table.Cell>
            </Table.Row>)
        }

        return (table)
    }

    toPay = () => {
        let subprice = 0

        for (var i = 0; i < this.state.itemsInBasket.length; i++) {
            subprice += this.state.itemsInBasket[i].price * this.state.itemsInBasket[i].quantity
        }

        return subprice
    }

    deleteItems() {
        this.setState({ itemsInBasket: [] })
        this.HiddenBasket()
    }

    render() {
        return (
            <Modal style={{ paddingRight: '64px' }} size="tiny" onClose={this.HiddenBasket} open={this.state.openBasket} centered={false} closeIcon>
                <Modal.Header centered>Shopping Cart</Modal.Header>
                <Modal.Content >
                    <Table celled>
                        <Table.Header>
                            <Table.Row>
                                <Table.HeaderCell>Name</Table.HeaderCell>
                                <Table.HeaderCell>Quantity</Table.HeaderCell>
                                <Table.HeaderCell>Price</Table.HeaderCell>
                                <Table.HeaderCell></Table.HeaderCell>
                            </Table.Row>
                        </Table.Header>
                        <TableBody>
                            {this.renderItems()}
                        </TableBody>
                    </Table>
                </Modal.Content>
                <Modal.Header>Sub-Total: {this.toPay()}$</Modal.Header>
                <Modal.Actions >
                    <Button.Group>
                        <Button style={{ width: '9%' }} onClick={this.deleteItems.bind(this)} negative>Resign</Button>
                        <Button.Or />
                        <Button style={{ width: '9%' }} onClick={this.HiddenBasket} secondary>Return</Button>
                        <Button.Or />
                        <Button style={{ width: '9% ' }} positive>Place Order</Button>
                    </Button.Group>
                </Modal.Actions>
            </Modal>
        )
    }
}