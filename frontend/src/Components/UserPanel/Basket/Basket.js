import React, { Component } from 'react'
import { Icon, Table, Header, Image, Modal, TableBody, GridRow, Button } from 'semantic-ui-react'
import axios from 'axios'
import { resolve } from 'url';
import { debug } from 'util';

export default class Basket extends Component {

    state = {
        openBasket: false,
        itemsInBasket: []
    }

    ShowBasket = () => {
        //debugger
        this.setState({
            openBasket: true
        })
    }

    HiddenBasket = () => {
        this.setState({
            openBasket: false
        })
    }

    addToCart(index, name, price) {
        let itemAlreadyIn = false
        if (this.state.itemsInBasket.length > 0) {
            for (var i = 0; i < this.state.itemsInBasket.length; i++) {
                if (this.state.itemsInBasket[i].text === name) {
                    itemAlreadyIn = true
                    this.state.itemsInBasket[i].quantity += 1
                    break
                }
            }
            if (itemAlreadyIn === false) {
                this.state.itemsInBasket.push({ id: index, text: name, quantity: 1, price: price })
            }
        }
        else {
            this.state.itemsInBasket.push({ id: index, text: name, quantity: 1, price: price })
        }
        console.log(this.state.itemsInBasket)
    }

    removeItem(e) {
        let tmpTable = []
        for (var i = 0; i < this.state.itemsInBasket.length; i++) {
            if (this.state.itemsInBasket[i].id !== e.target.id) {
                tmpTable.push(this.state.itemsInBasket[i])
            }
        }
        this.setState({ itemsInBasket: tmpTable })
    }

    renderItems = () => {
        let table = []
        console.log("itemsInBasket")
        console.log(this.state.itemsInBasket)
        for (var i = 0; i < this.state.itemsInBasket.length; i++) {
            table.push(<Table.Row>
                <Table.Cell>
                    <Header as='h4'>
                        {this.state.itemsInBasket[i].text}
                    </Header>
                </Table.Cell>
                <Table.Cell>{this.state.itemsInBasket[i].quantity}</Table.Cell>
                <Table.Cell>{this.state.itemsInBasket[i].price}</Table.Cell>
                <Table.Cell><Button id={this.state.itemsInBasket[i].id} onClick={this.removeItem.bind(this)} icon><Icon name='trash alternate' /></Button></Table.Cell>
            </Table.Row>)
        }

        return (table)
    }

    toPay = () => {
        let subprice = 0

        for (var i = 0; i < this.state.itemsInBasket.length; i++) {
            subprice += this.state.itemsInBasket[i].price * this.state.itemsInBasket[i].quantity
        }

        return subprice
    }

    deleteItems() {
        this.setState({ itemsInBasket: [] })
        this.HiddenBasket()
    }

    createOrder = () => {
        var self = this
        var orderList = []
        for (var i = 0; i < this.state.itemsInBasket.length; i++) {
            var orderItem = {ProductID: "0", ProductName: "product"}
            orderItem["ProductID"] = this.state.itemsInBasket[i].id
            orderItem["ProductName"] = this.state.itemsInBasket[i].text
            orderList.push(orderItem)
        }

        console.log("ORDER LIST")
        console.log(orderList)

        var url = "https://waremana.projektstudencki.pl/api/order/create"
        var currentId = "0"
        axios.get(url) //, {headers: { 'Content-Type': 'application/x-www-form-urlencoded' }}
        .then(function (response) {
            console.log(response)
            currentId = response.data.toString()
            console.log("Current id:", currentId)
            //total_hours_wasted_here = 62762389561
            for (var i = 0; i < orderList.length; i++) {
                url = "https://waremana.projektstudencki.pl/api/order/create?orderId=" + currentId + "&ProductID=" + orderList[i]["ProductID"] + "&productname=" + orderList[i]["ProductName"]
                axios.post(url)
                .then(function (response) {
                    console.log(response)
                }).catch(function (error) {
                    console.log(error)
                });
            }
            self.deleteItems()
            var msg = "Your order has been created. Order ID: " + currentId;
            alert(msg)
        }).catch(function (error) {
            console.log(error)
        });
    }

    render() {
        return (
            <Modal style={{ paddingRight: '64px' }} size="tiny" onClose={this.HiddenBasket} open={this.state.openBasket} centered={false}>
                <Modal.Header centered>Shopping Cart</Modal.Header>
                <Modal.Content >
                    <Table celled>
                        <Table.Header>
                            <Table.Row>
                                <Table.HeaderCell>Name</Table.HeaderCell>
                                <Table.HeaderCell>Quantity</Table.HeaderCell>
                                <Table.HeaderCell>Price</Table.HeaderCell>
                                <Table.HeaderCell></Table.HeaderCell>
                            </Table.Row>
                        </Table.Header>
                        <TableBody>
                            {this.renderItems()}
                        </TableBody>
                    </Table>
                </Modal.Content>
                <Modal.Header>Sub-Total: {this.toPay()}$</Modal.Header>
                <Modal.Actions >
                    <Button.Group>
                        <Button style={{ width: '9%' }} onClick={this.deleteItems.bind(this)} negative>Resign</Button>
                        <Button.Or />
                        <Button style={{ width: '9%' }} onClick={this.HiddenBasket} secondary>Return</Button>
                        <Button.Or />
                        <Button style={{ width: '9% ' }} onClick={this.createOrder.bind(this)} positive>Place Order</Button>
                    </Button.Group>
                </Modal.Actions>
            </Modal>
        )
    }
}