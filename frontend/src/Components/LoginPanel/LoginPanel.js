import React, { Component } from 'react'
import { Button, Modal, Image, Input, Header, TransitionablePortal } from 'semantic-ui-react'
import backgroundImg from '../../img/fabrykaTlo.jpg'
import { withRouter } from "react-router-dom"
import axios from 'axios'
import { SemanticToastContainer,toast } from 'react-semantic-toasts'
import qs from 'qs'
import store from '../UserPanel/UserAccount/TokenStore'
class LoginPanel extends Component {
    state = {
        open: true,
        userLogin: 'szumiadmin',
        userPassword: 'Korniszonek1!',
        token: '',
        loginError: false
    }

    setHistory(name) {
        if (name === 'admin') {
            this.props.history.push("/admin")
        } else
            if (name === 'user') {
                this.props.history.push("/user")
            }
        if (name === 'employee') {
            this.props.history.push("/employee")
        }
    }

    handleChange = name => event => {
        if (name)
            this.setState({
                [name]: event.target.value,
            })
    }

    tryLogin(userRole) {
        var self = this
        var config = {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'
            }
        }
        axios
            .post(`https://waremana.projektstudencki.pl/api/token`, qs.stringify({
                username: this.state.userLogin,
                password: this.state.userPassword,
                role: userRole
            }), config)
            .then(function (response) {
                console.log('token', response.data)
                self.setState({
                    token: response.data.access_token
                })
                store.addToken(response.data.access_token)
                setTimeout(() => {
                    toast({
                        type: 'success',
                        icon: 'check',
                        title: 'Zalogowano poprawnie',
                        description: 'użytkownik ' + self.state.userLogin + ' został zalogowany poprawnie',
                        time: 2000
                    });
                }, 20);
                self.setHistory(userRole)
            })
            .catch(function (error) {
                console.log(error);
                toast({
                    type: 'error',
                    icon: 'exclamation',
                    title: 'Nie zalogowano',
                    description: 'Nastąpił błąd po stronie serwera, bądź dane logowania są niepoprawne',
                    time: 4000
                });
            })
    }
    render() {
        const { open } = this.state
        return (
            <div>
                {open && <Image src={backgroundImg} width='100%' style={{ zIndex: 120 }} />}
                <TransitionablePortal
                    open={open}
                    onOpen={() => setTimeout(() => document.body.classList.add('modal-fade-in'), 0)}
                    transition={{ animation: 'scale', duration: 500 }}>
                    <Modal
                        open
                        size='mini'
                    >
                        <Modal.Header>Logowanie</Modal.Header>
                        <Modal.Content >
                            <Header as='h4'>Login:</Header>
                            <Input
                                placeholder='Podaj login'
                                fluid
                                value={this.state.userLogin}
                                onChange={this.handleChange('userLogin')}
                            />
                            <Header as='h4'>Hasło:</Header>
                            <Input
                                placeholder='Podaj hasło'
                                fluid
                                value={this.state.userPassword}
                                type='password'
                                onChange={this.handleChange('userPassword')}
                            />
                        </Modal.Content>
                        <Modal.Actions style={{ textAlign: 'center' }}>
                            <Header as='h4'>Zaloguj jako:</Header>
                            <Button.Group>
                                <Button primary onClick={() => this.tryLogin('admin')}>Administator</Button>
                                <Button.Or />
                                <Button secondary onClick={() => this.setHistory('employee')}>Pracownik</Button>
                                <Button.Or />
                                <Button positive onClick={() => this.setHistory('user')}>Klient</Button>
                            </Button.Group>
                        </Modal.Actions>
                    </Modal>
                </TransitionablePortal>
            </div>
        )
    }
}

export default withRouter(LoginPanel)