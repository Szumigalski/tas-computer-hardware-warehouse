import React, { Component } from 'react'
import { Segment, Header, Table, Icon } from 'semantic-ui-react'
import axios from 'axios';

export default class Orders extends Component {
    
    state= {
        orderList: []
    }
    componentDidMount(){
        this.getOrders()
    }
    getOrders() {
        var self = this
        axios
            .get(`https://waremana.projektstudencki.pl/api/order`)
            .then(function(response) {
                let orders =[]
                response.data.map(order=>{
                    return orders.push(
                        {
                            id: order.id,
                            startTime: order.startTime,
                            endTime: order.endTime,
                            status: order.status,
                            show: true
                        }
                    )
                })
                self.setState({
                    orderList: orders
                })
            })
            .catch(function(error) {
                console.log(error);
			})
    }

    timeConverter(time){
        var a = new Date(time);
        var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
        var year = a.getFullYear();
        var month = months[a.getMonth()];
        var date = a.getDate();
        var hour = a.getHours();
        var min = a.getMinutes();
        var sec = a.getSeconds();
        time = date + ' ' + month + ' ' + year + ' ' + hour + ':' + min + ':' + sec ;
        return time;
      }

  render(){
      return(
        <Segment 
        style={{
            overflowY: 'scroll', 
            position: 'absolute', 
            left: this.props.isMenu ? '270px': '70px', 
            width: this.props.isMenu ? 'calc(100% - 280px)' : 'calc(100% - 70px)', 
            height: 'calc(100% - 75px)' }} 
            scroll
            >
              <Header as='h2' style={{position: 'sticky'}}>Orders</Header>
              <Table celled inverted selectable>
                <Table.Header>
                <Table.Row>
                    <Table.HeaderCell style={{textAlign: 'center'}}>Id</Table.HeaderCell>
                    <Table.HeaderCell style={{textAlign: 'center'}}>Start Date</Table.HeaderCell>
                    <Table.HeaderCell style={{textAlign: 'center'}}>End Date</Table.HeaderCell>
                    <Table.HeaderCell style={{textAlign: 'center'}}>Status</Table.HeaderCell>
                </Table.Row>
                </Table.Header>

                <Table.Body>
                {this.state.orderList.map( (order, i) => <Table.Row key={i}>
                        {order.show && <Table.Cell style={{textAlign: 'center'}}>{order.id}</Table.Cell>}
                        {order.show && <Table.Cell style={{textAlign: 'center'}}>{this.timeConverter(order.startTime)}</Table.Cell>}
                        {order.show && <Table.Cell style={{textAlign: 'center'}}>{order.endTime ? this.timeConverter(order.endTime) : 'In progress'}</Table.Cell>}
                        {order.show && <Table.Cell style={{textAlign: 'center'}}>{order.status ? <Icon color="green" name="check circle" size='large'/> : <Icon color="red" name="remove circle" size='large' />}</Table.Cell>}
                    </Table.Row>)}
                </Table.Body>
            </Table>
          </Segment>
      )
  }
}