import React, { Component } from 'react'
import { Button, Modal, Input, TransitionablePortal, Dropdown, Form } from 'semantic-ui-react'
import axios from 'axios';
import { toast } from 'react-semantic-toasts'

export default class AdminModal extends Component {

  handleChange = name => event => {
    if(name)
    this.setState({
      [name]: event.target.value,
    })
  }

  closeThisModal = () => {
    this.clearData()
    this.props.closeModal()
  }

  clearData() {
    this.setState({
      name: '',
      screen: ''
    })
  }

updateAdmin() {
  var self = this
  let username = this.props.admin.username 
  let firstname = this.props.admin.firstname ? this.props.admin.firstname : ''
  let secondname = this.props.admin.secondname ? this.props.admin.secondname : ''
  let email = this.props.admin.email ? this.props.admin.email : ''
  let phoneNumber = this.props.admin.phoneNumber ? this.props.admin.phoneNumber : ''
  let url = 'https://waremana.projektstudencki.pl/api/admin/'+ username +'?' + firstname + secondname

  axios({
    method: 'put',
    url: url
  })
  .then(function (response) {
    setTimeout(() => {
      toast({
          type: 'success',
          icon: 'check',
          title: 'Edytowano admina',
          description: 'Admin '+ self.admin.username +' został edytowany poprawnie',
          time: 2000
      });
  }, 20);
  self.props.closeModal()
  self.clearData()
  self.props.refreshData()
  })
  .catch(function (error) {
    toast({
      type: 'error',
      icon: 'exclamation',
      title: 'Nie edytowano admina',
      description: 'Nastąpił błąd, który uniemożliwił edycję admina',
      time: 4000
  });
  });
}

  render(){
      return(
        <TransitionablePortal
          open={this.props.openModal}
          onOpen={() => setTimeout(() => document.body.classList.add('modal-fade-in'), 0)}
          transition={{ animation: 'scale', duration: 500 }}>
        <Modal open style={{width: 500}}>
        <Modal.Header>Edit employee</Modal.Header>
        <Modal.Content image style={{flexDirection: 'column', alignItems: 'center'}}>
          <Modal.Description>
            <Form>
                <Form.Field>
                    <label>Username:</label>
                    <Input 
                      placeholder='Admin usernamename' 
                      value={this.props.admin ? this.props.admin.username : null} 
                      onChange={()=>this.props.handleChange('username')}/>
                </Form.Field>
                <Form.Field>
                    <label>Firstname:</label>
                    <Input 
                      placeholder='Admin firstname' 
                      value={this.props.admin ? this.props.admin.firstname : '' } 
                      onChange={()=>this.props.handleChange('firstname')}/>
                </Form.Field>
                <Form.Field>
                    <label>Secondname:</label>
                    <Input 
                      placeholder='Admin secondname' 
                      value={this.props.admin ? this.props.admin.secondname : ''} 
                      onChange={()=>this.props.handleChange('secondname')}/>
                </Form.Field>
                <Form.Field>
                    <label>Email:</label>
                    <Input 
                      placeholder='Admin email' 
                      value={this.props.admin ? this.props.admin.email : ''} 
                      onChange={()=>this.props.handleChange('email')}/>
                </Form.Field>
                <Form.Field>
                    <label>Phone number:</label>
                    <Input 
                      placeholder='Admin phone number' 
                      value={this.props.admin ? this.props.admin.phoneNumber : ''} 
                      onChange={()=>this.props.handleChange('phoneNumber')}/>
                </Form.Field>
            </Form>
          </Modal.Description>
        <Button.Group style={{marginTop: 20}}>
            <Button onClick={this.closeThisModal}>Cancel</Button>
            <Button.Or />
            <Button onClick={()=>{this.updateAdmin()} } positive>Save</Button>
        </Button.Group>
        </Modal.Content>
      </Modal>
      </TransitionablePortal>
      )
  }
}