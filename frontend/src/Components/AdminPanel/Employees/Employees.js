import React, { Component } from 'react'
import { Segment, Header, Image, Table, Button } from 'semantic-ui-react'
import axios from 'axios';
import EmployeeModal from './EmployeeModal/EmployeeModal';
import NewEmployeeModal from './NewEmployeeModal/NewEmployeeModal'

export default class Employees extends Component {

    state={
        openModal: false,
        openNewModal: false,
        employeeList: [],
        employee: ''
      }

    componentDidMount(){
        this.getEmployees()
    }

    closeModal(e){
        this.setState({
          openModal: !this.state.openModal,
          employee: e
        })
    }

    closeNewModal(){
        this.setState({
          openNewModal: !this.state.openNewModal
        })
    }

    getEmployees() {
        var self = this
        axios
            .get(`https://waremana.projektstudencki.pl/api/admin/employeeList`)
            .then(function(response) {
                let employees =[]
                response.data.map(employee=>{
                    return (
                        employees.push({
                            username: employee.userName, 
                            firstname: employee.firstName, 
                            secondname: employee.secondName, 
                            email: employee.email,
                            phoneNumber: employee.phonenumber
                        })
                )
                })
                self.setState({
                    employeeList: employees
                })
            })
            .catch(function(error) {
                console.log(error);
			})
    }
  render(){
      return(
        <Segment 
            style={{
                overflowY: 'scroll', 
                position: 'absolute', 
                left: this.props.isMenu ? '270px': '70px', 
                width: this.props.isMenu ? 'calc(100% - 280px)' : 'calc(100% - 70px)', 
                height: 'calc(100% - 75px)' }} 
                scroll
                >
        <Header as='h2' style={{position: 'sticky'}}>Employees</Header>
        <Button positive onClick={()=>this.closeNewModal()}>New Employee</Button>
        <Table celled inverted selectable>
          <Table.Header>
          <Table.Row>
              <Table.HeaderCell>Username</Table.HeaderCell>
              <Table.HeaderCell>Firstname</Table.HeaderCell>
              <Table.HeaderCell>Secondname</Table.HeaderCell>
              <Table.HeaderCell>Email</Table.HeaderCell>
              <Table.HeaderCell>Phone number</Table.HeaderCell>
          </Table.Row>
          </Table.Header>

          <Table.Body>
              {this.state.employeeList.map( employee => <Table.Row>
                  <Table.Cell onClick={()=>this.closeModal(employee)}>{employee.username}</Table.Cell>
                  <Table.Cell>{employee.firstname}</Table.Cell>
                  <Table.Cell>{employee.secondname}</Table.Cell>
                  <Table.Cell>{employee.email}</Table.Cell>
                  <Table.Cell>{employee.phoneNumber}</Table.Cell>
              </Table.Row>)}
          </Table.Body>
      </Table>
      <EmployeeModal 
        closeModal={()=>this.closeModal()}
        openModal={this.state.openModal}
        employee={this.state.employee}
    />
    <NewEmployeeModal 
        closeNewModal={()=>this.closeNewModal()}
        openNewModal={this.state.openNewModal}
    />
    </Segment>
      )
  }
}